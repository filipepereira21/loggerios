VERSION=$1
BUILD_VERSION=$2
BUILD_VERSION_DEFAULT="1.0"

if [ -z "$VERSION" ]; then
  echo "Version is undefined"
  exit 1
fi

if [ -z "$BUILD_VERSION" ]; then
	echo "BUILD VERSION SETTED"
  BUILD_VERSION=BUILD_VERSION_DEFAULT
fi

echo "Looking for current version"
xcrun agvtool what-marketing-version

echo "Looking for current build version"
xcrun agvtool what-version

echo Updating version
xcrun agvtool new-marketing-version $VERSION

echo Updating build version
xcrun agvtool new-version -all $BUILD_VERSION


