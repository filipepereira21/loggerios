//
//  Logger.swift
//  Logger
//
//  Created by Filipe Pereira on 05/11/2019.
//  Copyright © 2019 Filipe Pereira. All rights reserved.
//

import Foundation

protocol Logger {
    static func log(message: String)
}
