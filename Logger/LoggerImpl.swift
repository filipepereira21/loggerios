//
//  LoggerImpl.swift
//  Logger
//
//  Created by Filipe Pereira on 05/11/2019.
//  Copyright © 2019 Filipe Pereira. All rights reserved.
//

import Foundation

public class LoggerImpl: Logger {
    
    public static func log(message: String) {
        print(message)
    }
    
}
