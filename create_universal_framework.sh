REVEAL_ARCHIVE_IN_FINDER=true
PROJECT_NAME="Logger"
PROJECT_DIR=$(pwd)
CONFIGURATION="Release"
UNIVERSAL_OUTPUT_DIR="$PROJECT_DIR/$CONFIGURATION"
DEVICE_BUILD_DIR="$PROJECT_DIR/build_device"
SIMULATOR_BUILD_DIR="$PROJECT_DIR/build_simulator"

# Make sure the output directory exists
if [ -d $UNIVERSAL_OUTPUT_DIR ]; then
  rm -r $UNIVERSAL_OUTPUT_DIR
fi

mkdir $UNIVERSAL_OUTPUT_DIR

# Next, work out if we're in SIM or DEVICE
xcodebuild -configuration ${CONFIGURATION} -target ${PROJECT_NAME} -sdk iphonesimulator ONLY_ACTIVE_ARCH=NO CONFIGURATION_BUILD_DIR=${SIMULATOR_BUILD_DIR} clean build
xcodebuild -configuration ${CONFIGURATION} -target ${PROJECT_NAME} -sdk iphoneos ONLY_ACTIVE_ARCH=NO CONFIGURATION_BUILD_DIR=${DEVICE_BUILD_DIR} clean build

# Step 2. Copy the framework structure (from iphoneos build) to the universal folder
cp -r "$DEVICE_BUILD_DIR/$PROJECT_NAME.framework" "$UNIVERSAL_OUTPUT_DIR/"

# Step 3. Copy Swift modules from iphonesimulator build (if it exists) to the copied framework directory
cp -r "$SIMULATOR_BUILD_DIR/$PROJECT_NAME.framework/Modules/$PROJECT_NAME.swiftmodule/" "$UNIVERSAL_OUTPUT_DIR/$PROJECT_NAME.framework/Modules/$PROJECT_NAME.swiftmodule"

# Step 4. Create universal binary file using lipo and place the combined executable in the copied framework directory
lipo -create -output "$UNIVERSAL_OUTPUT_DIR/$PROJECT_NAME.framework/$PROJECT_NAME" "$DEVICE_BUILD_DIR/$PROJECT_NAME.framework/$PROJECT_NAME" "$SIMULATOR_BUILD_DIR/$PROJECT_NAME.framework/$PROJECT_NAME"

# Step 5. Removing created directories
rm -r "$PROJECT_DIR/build"
rm -r $DEVICE_BUILD_DIR
rm -r $SIMULATOR_BUILD_DIR